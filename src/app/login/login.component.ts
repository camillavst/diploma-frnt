import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup} from "@angular/forms";
import {IAuthResponse, ILoginData} from "../utils";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public authDataForm = new FormGroup({
        email: new FormControl(),
        password: new FormControl(),
    });

    constructor(private router: Router,
                private authService: AuthService) {
    }

    ngOnInit(): void {
    }

    onSubmit(): void {
        const body: ILoginData = this.authDataForm.getRawValue();

        this.authService
            .login(body)
            .then((response: IAuthResponse) => {
                this.authService.setLoginResponse(response);

                this.router
                    .navigate(['/main/products'])
                    .then(() => true);
            })
            .catch();
    }
}
