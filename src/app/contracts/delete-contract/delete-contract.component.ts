import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {EURLS, IContract, IProduct} from "../../utils";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {HttpHeaders} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {ApiService} from "../../services/api.service";

@Component({
    selector: 'app-delete-contract',
    templateUrl: './delete-contract.component.html',
    styleUrls: ['./delete-contract.component.scss']
})
export class DeleteContractComponent implements OnInit {
    private contract: IContract;

    @Input('contract') set _product(input: IContract) {
        if (!input) {
            this.contract = undefined;
            return;
        }
        else {
            this.contract = input;
        }
    }

    constructor(private apiService: ApiService,
                private authService: AuthService) {
    }


    ngOnInit() {
    }

    @Output() onDeleted = new EventEmitter<IContract>();

    public onCancel(modal: NgxSmartModalComponent) {
        modal.close();
    }

    public onDelete(modal: NgxSmartModalComponent) {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .delete(`${EURLS.Contracts}${this.contract.id}`, requestHeaders)
            .then((response: IContract) => {
                this.onDeleted.emit(this.contract);
                modal.close();
            })
            .catch(error => modal.close());
    }
}
