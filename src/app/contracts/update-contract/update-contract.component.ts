import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {EURLS, IContract, IProduct} from "../../utils";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {HttpHeaders} from "@angular/common/http";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-update-contract',
    templateUrl: './update-contract.component.html',
    styleUrls: ['./update-contract.component.scss']
})
export class UpdateContractComponent implements OnInit {
    private contract: IContract;

    public updateContractForm = new FormGroup({});

    @Input('contract') set _product(input: IContract) {
        if (!input) {
            this.contract = undefined;
            return;
        }
        else {
            this.contract = input;
        }
    }

    constructor(private apiService: ApiService,
                private authService: AuthService) {
    }

    ngOnInit() {
    }

    @Output() onUpdated = new EventEmitter<IContract>();

    onSubmit(modal: NgxSmartModalComponent) {
        const body: IProduct = this.updateContractForm.getRawValue();
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .put(`${EURLS.Contracts}${this.contract.id}`, body, requestHeaders)
            .then((response: IContract) => {
                this.onUpdated.emit(response);
                modal.close();
            });
    }
}
