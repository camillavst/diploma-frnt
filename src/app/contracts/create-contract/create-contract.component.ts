import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormArray, FormControl, FormGroup} from "@angular/forms";
import {EURLS, IContract, ICustomer, IManager, IProduct, IReservation} from "../../utils";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {HttpHeaders} from "@angular/common/http";
import {AuthService} from "../../services/auth.service";
import {ApiService} from "../../services/api.service";

@Component({
    selector: 'app-create-contract',
    templateUrl: './create-contract.component.html',
    styleUrls: ['./create-contract.component.scss']
})
export class CreateContractComponent implements OnInit {
    availableCustomers: ICustomer[] = [];
    availableReservations: IReservation[] = [];

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    public selectedReservationsIds: string[] = [];

    public availableReservationsFormArray = new FormArray([]);

    public createContractForm = new FormGroup({});

    public isFormReady = false;

    ngOnInit() {
        this.getAvailableReservations();
        this.getAvailableCustomers();
    }

    getAvailableReservations() {
        const headers: HttpHeaders = this.authService.getRequestHeaders();
        const manager: IManager = this.authService.getUser();

        this.apiService
            .get(EURLS.AvailableReservations + manager.id, headers)
            .then((response: IReservation[]) => {
                this.availableReservations = [...response];
                this.setAvailableReservationsFormControl();
            });
    }

    setAvailableReservationsFormControl() {
        const array = this.availableReservations.map(reservation => {
            return new FormControl(false)
        });

        this.createContractForm = new FormGroup({
            number: new FormControl(),
            date: new FormControl(),
            contractor: new FormControl(),
            reservationIds: new FormArray(array),
            customerId: new FormControl(),
            deliveryConditions: new FormControl(),
            paymentConditions: new FormControl()
        });

        this.isFormReady = true;

        // this.createContractForm.addControl('reservationIds', this.availableReservationsFormArray);

        console.log(this.createContractForm)
        // console.log(this.availableReservationsFormArray)

    }

    getAvailableCustomers() {
        const headers: HttpHeaders = this.authService.getRequestHeaders();
        const manager: IManager = this.authService.getUser();

        this.apiService
            .get(EURLS.Customers, headers)
            .then((response: ICustomer[]) => {
                this.availableCustomers = [...response];
            });
    }

    @Output() onCreated = new EventEmitter<IContract>();

    onSubmit(modal: NgxSmartModalComponent): void {
        const body: IContract = this.createContractForm.getRawValue();
        const headers: HttpHeaders = this.authService.getRequestHeaders();

        const states = [...body.reservationIds];
        body.reservationIds = [];

        states.forEach((state, index) => {
            if(state){
                body.reservationIds.push(this.availableReservations[index].id);
            }
        });

        this.apiService
            .post(EURLS.Contracts, body, headers)
            .then((response: IContract) => {
                this.onCreated.emit(response);
                modal.close();
            });
    }
}
