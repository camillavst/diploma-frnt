import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {ApiService} from "../services/api.service";
import {EURLS, IContract} from "../utils";
import {HttpHeaders} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";
import {NgxSmartModalService} from "ngx-smart-modal";

@Component({
    selector: 'app-contracts',
    templateUrl: './contracts.component.html',
    styleUrls: ['./contracts.component.scss']
})
export class ContractsComponent implements OnInit {
    public isListEmpty: boolean = true;

    public contracts: IContract[] = [];
    public contractForDelete: IContract;
    public contractForUpdate: IContract;
    private contractForDownload: IContract;

    public tableHeaders = [
        {fieldName: 'number', title: 'Number'},
        {fieldName: 'date', title: 'Date'},
        {fieldName: 'creator', title: 'Creator'},
        {fieldName: 'customer', title: 'Customer'},
        {fieldName: 'paymentConditions', title: 'Payment conditions'},
        {fieldName: 'deliveryConditions', title: 'Delivery Conditions'}
    ];

    constructor(private ngxSmartModalService: NgxSmartModalService,
                private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
        this.onRead();
    }

    public onCreate(): void {
        this.ngxSmartModalService.getModal('createContractModal').open();
    }

    public onCreated(contract: IContract): void {
        this.contracts.push(contract);
    }

    private onRead() {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .get(EURLS.Contracts, requestHeaders)
            .then((response: IContract[]) => this.contracts = [...response])
            .then(() => this.isListEmpty = !this.contracts.length);
    }

    public onUpdate(contract: IContract): void {
        this.contractForUpdate = contract;
        this.ngxSmartModalService.getModal('updateContractModal').open();
    }

    public onUpdated(contract: IContract): void {
        const updatedIndex = this.contracts.findIndex(oldContract => oldContract.id === contract.id);
        this.contracts[updatedIndex] = contract;
    }

    public onDelete(contract: IContract): void {
        this.contractForDelete = contract;
        this.ngxSmartModalService.getModal('deleteContractModal').open();
    }

    public onDeleted(contract: IContract): void {
        const deletedIndex = this.contracts.findIndex(oldContract => oldContract.id === contract.id);
        this.contracts.splice(deletedIndex, 1);
    }

    public onDownload(contract: IContract): void {
        this.contractForDownload = contract;
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .download(`${EURLS.Contracts}${this.contractForDownload.id}/print`, requestHeaders)
            .then(response => {
                const blob = new Blob([response], {type: "application/pdf"});
                const url = window.URL.createObjectURL(blob);
                window.open(url);
            });
    }
}
