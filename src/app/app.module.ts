import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {ProductsComponent} from './products/products.component';
import {MainComponent} from './main/main.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {AuthService} from "./services/auth.service";
import {ApiService} from "./services/api.service";

import {AuthGuard} from "./services/auth.guard";
import {ContractsComponent} from './contracts/contracts.component';
import {CustomersComponent} from './customers/customers.component';
import {ReservationsComponent} from './reservations/reservations.component';
import {ManagersComponent} from './managers/managers.component';
import {CreateCustomerComponent} from './customers/create-customer/create-customer.component';
import {UpdateCustomerComponent} from './customers/update-customer/update-customer.component';
import {NgxSmartModalModule} from "ngx-smart-modal";
import {DeleteCustomerComponent} from "./customers/delete-customer/delete-customer.component";
import {UpdateProductComponent} from './products/update-product/update-product.component';
import {DeleteProductComponent} from './products/delete-product/delete-product.component';
import {CreateProductComponent} from './products/create-product/create-product.component';
import {ToastrModule} from "ngx-toastr";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CreateContractComponent} from './contracts/create-contract/create-contract.component';
import {DeleteContractComponent} from './contracts/delete-contract/delete-contract.component';
import {UpdateContractComponent} from './contracts/update-contract/update-contract.component';
import {NgHttpLoaderModule} from "ng-http-loader";
import {CommonModule} from "@angular/common";
import {CreateReservationComponent} from './reservations/create-reservation/create-reservation.component';
import {UpdateReservationComponent} from './reservations/update-reservation/update-reservation.component';
import {DeleteReservationComponent} from './reservations/delete-reservation/delete-reservation.component';
import {NgxMaskModule} from "ngx-mask";
import { CreateManagerComponent } from './managers/create-manager/create-manager.component';


@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        ProductsComponent,
        MainComponent,
        PageNotFoundComponent,
        ContractsComponent,
        CustomersComponent,
        ReservationsComponent,
        ManagersComponent,
        CreateCustomerComponent,
        UpdateCustomerComponent,
        DeleteCustomerComponent,
        UpdateProductComponent,
        DeleteProductComponent,
        CreateProductComponent,
        CreateContractComponent,
        DeleteContractComponent,
        UpdateContractComponent,
        CreateReservationComponent,
        UpdateReservationComponent,
        DeleteReservationComponent,
        CreateManagerComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        NgxSmartModalModule.forRoot(),
        BrowserAnimationsModule,
        ToastrModule.forRoot({
            timeOut: 2500,
            positionClass: 'toast-top-center',
            maxOpened: 3,
            countDuplicates: true,
            autoDismiss: true
        }),
        NgHttpLoaderModule.forRoot(),
        NgxMaskModule.forRoot()
    ],
    providers: [
        ApiService,
        AuthService,
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
