import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {HttpHeaders} from "@angular/common/http";
import {EURLS, IAuthResponse, IRefreshTokenResponse, ITokens} from "../utils";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private localStorage: Storage = localStorage;

    constructor(private apiService: ApiService) {
    }

    async login(body) {
        return await this.apiService.post(`${EURLS.Authorization}login`, body);
    }

    async isLoginned() {
        const storagedTokens = this.localStorage.getItem('tokens');
        const tokens: ITokens = storagedTokens ? JSON.parse(storagedTokens) as ITokens : null;

        if (!tokens || tokens && !tokens.access) return false;

        if (!AuthService.isExpired(tokens.access)) return true;

        if (AuthService.isExpired(tokens.refresh)) return false;

        try {
            const refreshTokenResponse = <IRefreshTokenResponse>await this.refreshToken(tokens.refresh.token);

            this.localStorage.setItem('user', JSON.stringify(refreshTokenResponse.user));
            this.localStorage.setItem('tokens', JSON.stringify(refreshTokenResponse.tokens));

            return true;
        }
        catch (error) {
            console.log(error);
            return false;
        }
    }

    static isExpired({token, expires}) {
        const now = Date.now();
        const timeout = 60 * 1000;
        const expiresIn = new Date(expires).getTime();

        return (now + timeout) > expiresIn;
    }

    async refreshToken(token): Promise<IRefreshTokenResponse> {
        return;
    }

    setLoginResponse(loginResponse: IAuthResponse) {
        this.localStorage.setItem('user', JSON.stringify(loginResponse.user));
        this.localStorage.setItem('tokens', JSON.stringify(loginResponse.tokens));
    }

    private getTokens() {
        const storagedTokens = this.localStorage.getItem('tokens');
        const tokens: ITokens = storagedTokens ? JSON.parse(storagedTokens) as ITokens : null;
        return tokens;
    }

    public getRequestHeaders(): HttpHeaders {
        const tokens = this.getTokens() || {access: {token: ''}};

        return new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${tokens.access.token}`);
    }

    removeAuthData() {
        this.localStorage.removeItem('user');
        this.localStorage.removeItem('tokens');
    }

    getUser() {
        const userString = this.localStorage.getItem('user');

        if (userString) {
            return JSON.parse(userString);
        }

        return null;
    }
}
