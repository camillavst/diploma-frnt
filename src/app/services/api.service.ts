import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable, EMPTY, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {ToastrService} from "ngx-toastr";
import {IError} from "../utils";

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private http: HttpClient,
                private toastr: ToastrService) {
    }

    public async request(method: string, url: string, body = undefined, requestHeaders: HttpHeaders): Promise<any> {
        let options: any = {
            responseType: "json",
            observe: "body",
            headers: requestHeaders
        };

        if (body) options['body'] = body;

        return this.http
            .request(method, url, options)
            .pipe(map(ApiService.responseHandler.bind(this)))
            .toPromise()
            .catch(ApiService.errorHandler.bind(this))
            .catch((error: IError) => {
                this.toastr.error(error.message.en, 'Error');
                throw error;
            });
    }

    public async post(url: string, body = {}, requestHeaders: HttpHeaders = {} as HttpHeaders): Promise<any> {
        return await this.request('POST', url, body, requestHeaders);
    }

    public async get(url: string, requestHeaders: HttpHeaders = {} as HttpHeaders): Promise<any> {
        return await this.request('GET', url, undefined, requestHeaders);
    }

    public async delete(url: string, requestHeaders: HttpHeaders = {} as HttpHeaders): Promise<any> {
        return await this.request('DELETE', url, undefined, requestHeaders);
    }

    public async put(url: string, body = {}, requestHeaders: HttpHeaders = {} as HttpHeaders): Promise<any> {
        return await this.request('PUT', url, undefined, requestHeaders);
    }

    public async download(url: string, requestHeaders: HttpHeaders = {} as HttpHeaders): Promise<any> {
        let options: any = {
            responseType: 'blob',
            observe: 'body',
            headers: requestHeaders
        };

        return await this.http
            .get(url, options)
            .toPromise()
            .catch(ApiService.errorHandler.bind(this))
            .catch((error: IError) => {
                this.toastr.error(error.message.en, 'Error');
                throw error;
            });
    }

    static responseHandler(response) {
        if (response && response.error) {
            throw response.error;
        }
        return response && response.result ? response.result : response;
    }

    static errorHandler(error) {
        if (error && error.error) {
            error = error.error;

            if (error.constructor === String) {
                error = {
                    message: error
                };
            }

            let message = {
                ru: 'Техт ошибки не найден.'.toUpperCase(),
                en: 'Message of error not found.'.toUpperCase()
            };

            if (error.message) {
                switch (error.message.constructor) {
                    case Object:
                        message = {
                            ru: error.message['ru'].toUpperCase(),
                            en: error.message['en'].toUpperCase()
                        };
                        break;

                    case String:
                        message = {
                            ru: error.message.toUpperCase(),
                            en: error.message.toUpperCase()
                        };
                        break;

                    default:
                        message = {
                            ru: error.message,
                            en: error.message
                        };
                        break;
                }
            }

            error = {
                code: error.code,
                message: message
            };
        }

        if (error instanceof HttpErrorResponse) {
            error = {
                code: -31600,
                message: {
                    ru: 'Ошибка связи с сервером.'.toUpperCase(),
                    en: 'Error communicating with the server.'
                },
                data: error
            };
        }

        throw error;
    }
}
