import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {MainComponent} from "./main/main.component";
import {ProductsComponent} from "./products/products.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {AuthGuard} from "./services/auth.guard";
import {ManagersComponent} from "./managers/managers.component";
import {ContractsComponent} from "./contracts/contracts.component";
import {ReservationsComponent} from "./reservations/reservations.component";
import {CustomersComponent} from "./customers/customers.component";

const routes: Routes = [
    {path: '', redirectTo: '/main/products', pathMatch: 'full'},
    {path: 'login', component: LoginComponent},
    {
        path: 'main', component: MainComponent,
        canActivate: [AuthGuard],
        children: [
            {path: 'products', component: ProductsComponent},
            {path: 'customers', component: CustomersComponent},
            {path: 'reservations', component: ReservationsComponent},
            {path: 'contracts', component: ContractsComponent},
            {path: 'managers', component: ManagersComponent},
        ]
    },
    {path: '**', redirectTo: '/404', pathMatch: 'full'},
    {path: '404', component: PageNotFoundComponent},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
