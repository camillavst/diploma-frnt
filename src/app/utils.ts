export enum EURLS {
    Authorization = '/api/v1/auth/',
    Managers = '/api/v1/users/',
    Products = '/api/v1/products/',
    AvailableProducts = '/api/v1/products/available/',
    Customers = '/api/v1/customers/',
    AvailableCustomers = '/api/v1/customers/available/',
    Reservations = '/api/v1/reservations/',
    AvailableReservations = '/api/v1/reservations/available/',
    Contracts = '/api/v1/contracts/'
}

export interface IError {
    code: number;
    message: {
        ru: string;
        en: string;
    };
    data?: any;
}

export interface ILoginData {
    email: string;
    password: string;
}

export type IUserRole = 'user' | 'admin';

export interface IUser {
    id: string;
    email: string;
    name: string;
    role: IUserRole;
}

export interface IToken {
    token: string;
    expires: string;
}

export interface ITokens {
    access: IToken;
    refresh: IToken;
}

export interface IAuthResponse {
    user: IUser;
    tokens: ITokens;
}

export interface IRefreshTokenResponse extends IAuthResponse {
}

export interface IManager {
    id?: string;
    email: string;
    name: string;
    role: IUserRole;
}

export interface IProduct {
    id?: string;
    name: string;
    category: string;
    supplyNumber: number;
    buyPrice: number;
    sellPrice: number;
    buyAmount: number;
    currentAmount: number;
    validUntil: string;
    manager?: IManager;
}

export interface ICustomer {
    id?: string;
    name: string;
    inn: string;
    address: string;
    phone: string;
    directorFullName: string;
    bankAccount: string;
    bankName: string;
    mfo: string;
    oked: string;
    ndsPayerCode: string;
    manager?: IManager;
}

export interface IReservation {
    id?: string;
    productId: string;
    product?: IProduct;
    amount: number;
    dueDate: string;
    notes: string;
    managerId?: string;
    manager?: IManager;
}

export interface IContract {
    id?: string;
    number: string;
    date: string;
    paymentConditions: string;
    deliveryConditions: string;
    reservationIds: string[];
    reservations?: IReservation[];
    customerId: string;
    customer?: ICustomer;
    managerId: string;
    manager?: IManager;
}

export const ProductsCategories = [
    {title: 'REAGENTS', value: 'reagents', selected: false},
    {title: 'CONSUMABLES', value: 'consumables', selected: false},
    {title: 'EQUIPMENT', value: 'equipment', selected: false}
];

export const PaymentConditions = [
    {
        title: "100% ,что составляет 140 000 000 (Сто сорок миллионов) сум  на расчетный счет Поставщика в течение 30 (тридцати) дней со дня подписания настоящего Договора.",
        value: "100% ,что составляет 140 000 000 (Сто сорок миллионов) сум  на расчетный счет Поставщика в течение 30 (тридцати) дней со дня подписания настоящего Договора.",
    },
    {
        title: "15% ,что составляет 21 000 000 (двадцать один миллион) сум  на расчетный счет Поставщика в течение 10 (тридцати) дней со дня подписания настоящего Договора. Оставшаяся сумма Договора оплачивается в течении 60 дней со дня подписания счет фактуры-накладной",
        value: "15% ,что составляет 21 000 000 (двадцать один миллион) сум  на расчетный счет Поставщика в течение 10 (тридцати) дней со дня подписания настоящего Договора. Оставшаяся сумма Договора оплачивается в течении 60 дней со дня подписания счет фактуры-накладной"
    },
    {
        title: "100%, что составляет 140 000 000 (Сто сорок миллионов) сум, до 16/06/2020 года.",
        value: "100%, что составляет 140 000 000 (Сто сорок миллионов) сум, до 16/06/2020 года.",
    }
];

export const DeliveryConditions = [
    {
        title: "30 (тридцать) дней",
        value: "30 (тридцать) дней"
    },
    {
        title: "60 (шестьдесят) дней",
        value: "60 (шестьдесят) дней",
    },
    {
        title: "90 (девяносто) дней",
        value: "90 (девяносто) дней"
    }
];
