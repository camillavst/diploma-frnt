import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";
import {EURLS, ICustomer} from "../../utils";
import {HttpHeaders} from "@angular/common/http";
import {NgxSmartModalComponent} from "ngx-smart-modal";

@Component({
    selector: 'app-update-customer',
    templateUrl: './update-customer.component.html',
    styleUrls: ['./update-customer.component.scss']
})
export class UpdateCustomerComponent implements OnInit {
    private customer: ICustomer;

    @Input('customer') set _customer(input: ICustomer) {
        if (!input) {
            this.customer = undefined;
            return;
        }
        else {
            this.customer = input;
        }

        this.customerUpdatingForm.controls.id.setValue(input.id);
        this.customerUpdatingForm.controls.name.setValue(input.name);
        this.customerUpdatingForm.controls.inn.setValue(input.inn);
        this.customerUpdatingForm.controls.address.setValue(input.address);
        this.customerUpdatingForm.controls.phone.setValue(input.phone);
        this.customerUpdatingForm.controls.directorFullName.setValue(input.directorFullName);
        this.customerUpdatingForm.controls.bankAccount.setValue(input.bankAccount);
        this.customerUpdatingForm.controls.bankName.setValue(input.bankName);
        this.customerUpdatingForm.controls.mfo.setValue(input.mfo);
        this.customerUpdatingForm.controls.oked.setValue(input.oked);
        this.customerUpdatingForm.controls.ndsPayerCode.setValue(input.ndsPayerCode);
    }

    public customerUpdatingForm = new FormGroup({
        id: new FormControl(),
        name: new FormControl(),
        inn: new FormControl(),
        address: new FormControl(),
        phone: new FormControl(),
        directorFullName: new FormControl(),
        bankAccount: new FormControl(),
        bankName: new FormControl(),
        mfo: new FormControl(),
        oked: new FormControl(),
        ndsPayerCode: new FormControl()
    });

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit() {
    }

    @Output() onUpdated = new EventEmitter<ICustomer>();

    onSubmit(modal: NgxSmartModalComponent): void {
        const body: ICustomer = this.customerUpdatingForm.getRawValue();
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .put(`${EURLS.Customers}${this.customer.id}`, body, requestHeaders)
            .then((response: ICustomer) => {
                this.onUpdated.emit(response);
                modal.close();
            });
    }
}
