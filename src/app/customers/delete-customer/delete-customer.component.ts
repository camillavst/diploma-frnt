import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EURLS, ICustomer} from "../../utils";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {HttpHeaders} from "@angular/common/http";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-delete-customer',
    templateUrl: './delete-customer.component.html',
    styleUrls: ['./delete-customer.component.scss']
})
export class DeleteCustomerComponent implements OnInit {
    private customer: ICustomer;

    @Input('customer') set _customer(input: ICustomer) {
        if (!input) {
            this.customer = undefined;
            return;
        }
        else {
            this.customer = input;
        }
    }

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    onCancel(modal: NgxSmartModalComponent): void {
        modal.close();
    }

    ngOnInit() {
    }

    @Output() onDeleted = new EventEmitter<ICustomer>();

    onDelete(modal: NgxSmartModalComponent): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .delete(`${EURLS.Customers}${this.customer.id}`, requestHeaders)
            .then((response: ICustomer) => {
                this.onDeleted.emit(this.customer);
                modal.close();
            })
            .catch(error => modal.close());;
    }
}
