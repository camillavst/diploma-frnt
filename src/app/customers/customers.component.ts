import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {ApiService} from "../services/api.service";
import {NgxSmartModalService} from "ngx-smart-modal";
import {EURLS, ICustomer} from "../utils";
import {HttpHeaders} from "@angular/common/http";

@Component({
    selector: 'app-customers',
    templateUrl: './customers.component.html',
    styleUrls: ['./customers.component.scss']
})
export class CustomersComponent implements OnInit {
    public isListEmpty: boolean = true;

    public customers: ICustomer[] = [];

    public customerForUpdate: ICustomer;
    public customerForDelete: ICustomer;

    public tableHeaders = [
        {fieldName: 'name', title: 'Name'},
        {fieldName: 'inn', title: 'TIN'},
        {fieldName: 'address', title: 'Address'},
        {fieldName: 'phone', title: 'Phone'},
        {fieldName: 'directorFullName', title: 'Director Fullname'},
        {fieldName: 'bankName', title: 'Bank name'},
        {fieldName: 'bankAccount', title: 'Bank account'},
        {fieldName: 'mfo', title: 'MFO'},
        {fieldName: 'oked', title: 'OKED'},
        {fieldName: 'ndsPayerCode', title: 'VAT Payer Code'},
    ];

    constructor(public ngxSmartModalService: NgxSmartModalService,
                private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
        this.onRead();
    }

    public onRead(): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .get(EURLS.Customers, requestHeaders)
            .then((response: ICustomer[]) => this.customers = [...response])
            .then(() => this.isListEmpty = !this.customers.length)
            .catch(error => error);
    }

    public onCreate(): void {
        this.ngxSmartModalService.getModal('createCustomerModal').open();
    }

    public onCreated(customer: ICustomer): void {
        this.customers.push(customer);
    }

    public onUpdate(customer: ICustomer): void {
        this.customerForUpdate = customer;
        this.ngxSmartModalService.getModal('updateCustomerModal').open();
    }

    public onUpdated(customer: ICustomer): void {
        const updatedIndex = this.customers.findIndex(oldCustomer => oldCustomer.id === customer.id);
        this.customers[updatedIndex] = customer;
    }

    public onDelete(customer: ICustomer): void {
        this.customerForDelete = customer;
        this.ngxSmartModalService.getModal('deleteCustomerModal').open();
    }

    public onDeleted(customer: ICustomer): void {
        const deletedIndex = this.customers.findIndex(oldCustomer => oldCustomer.id === customer.id);
        this.customers.splice(deletedIndex, 1);
    }
}
