import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";
import {EURLS, ICustomer} from "../../utils";
import {HttpHeaders} from "@angular/common/http";
import {NgxSmartModalComponent} from "ngx-smart-modal";

@Component({
    selector: 'app-create-customer',
    templateUrl: './create-customer.component.html',
    styleUrls: ['./create-customer.component.scss']
})
export class CreateCustomerComponent {
    public customerCreateForm = new FormGroup({
        name: new FormControl(),
        inn: new FormControl(),
        address: new FormControl(),
        phone: new FormControl(),
        directorFullName: new FormControl(),
        bankAccount: new FormControl(),
        bankName: new FormControl(),
        mfo: new FormControl(),
        oked: new FormControl(),
        ndsPayerCode: new FormControl()
    });

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    @Output() onCreated = new EventEmitter<ICustomer>();

    public onSubmit(modal: NgxSmartModalComponent): void {
        const body: ICustomer = this.customerCreateForm.getRawValue();
        const headers: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .post(EURLS.Customers, body, headers)
            .then((response: ICustomer) => {
                this.onCreated.emit(response);
                modal.close();
            });
    }
}
