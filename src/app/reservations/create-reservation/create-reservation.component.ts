import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {NgxSmartModalComponent, NgxSmartModalService} from "ngx-smart-modal";
import {EURLS, IManager, IProduct, IReservation} from "../../utils";
import {HttpHeaders} from "@angular/common/http";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-create-reservation',
    templateUrl: './create-reservation.component.html',
    styleUrls: ['./create-reservation.component.scss']
})
export class CreateReservationComponent implements OnInit {
    public availableProducts: IProduct[] = [];
    public selectedProduct: IProduct;

    public createReservationForm = new FormGroup({
        productId: new FormControl(),
        amount: new FormControl(),
        dueDate: new FormControl(),
        notes: new FormControl()
    });

    constructor(public ngxSmartModalService: NgxSmartModalService,
                private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit() {
        this.getAvailableProducts();
    }

    @Output() onCreated = new EventEmitter<IReservation>();

    onSubmit(modal: NgxSmartModalComponent): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();
        const body = this.createReservationForm.getRawValue();

        this.apiService
            .post(`${EURLS.Reservations}`, body, requestHeaders)
            .then((response: IReservation) => {
                this.onCreated.emit(response);
                modal.close();
            });
    }

    getAvailableProducts() {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();
        const manager: IManager = this.authService.getUser();

        this.apiService
            .get(`${EURLS.AvailableProducts}${manager.id}`, requestHeaders)
            .then((response: IProduct[]) => this.availableProducts = [...response]);
    }

    onModalClosed() {
        this.selectedProduct = undefined;
        console.log(this.selectedProduct);
    }
}
