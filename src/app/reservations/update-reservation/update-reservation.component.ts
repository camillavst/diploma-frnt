import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {IReservation} from "../../utils";

@Component({
    selector: 'app-update-reservation',
    templateUrl: './update-reservation.component.html',
    styleUrls: ['./update-reservation.component.scss']
})
export class UpdateReservationComponent implements OnInit {
    private reservation: IReservation;
    public updateReservationForm = new FormGroup({});

    @Input('reservation') set _reservation(input: IReservation) {
        if (!input) {
            this.reservation = undefined;
            return;
        }
        else {
            this.reservation = input;
        }
    }

    constructor() {
    }

    ngOnInit() {
    }

    @Output() onUpdated = new EventEmitter<IReservation>();

    onSubmit(modal: NgxSmartModalComponent): void {

    }
}
