import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {ApiService} from "../services/api.service";
import {EURLS, IReservation} from "../utils";
import {HttpHeaders} from "@angular/common/http";
import {NgxSmartModalService} from "ngx-smart-modal";

@Component({
    selector: 'app-reservations',
    templateUrl: './reservations.component.html',
    styleUrls: ['./reservations.component.scss']
})
export class ReservationsComponent implements OnInit {
    public isListEmpty: boolean = true;

    public reservations: IReservation[] = [];
    public reservationForUpdate: IReservation;
    public reservationForDelete: IReservation;

    public tableHeaders = [
        {fieldName: 'Product name', title: 'Product name'},
        {fieldName: 'amount', title: 'Amout'},
        {fieldName: 'creator', title: 'Creator'},
        {fieldName: 'dueDate', title: 'Due Date'},
        {fieldName: 'notes', title: 'Notes'}
    ];

    constructor(private ngxSmartModalService: NgxSmartModalService,
                private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
        this.onRead();
    }

    public onCreate() {
        this.ngxSmartModalService.getModal('createReservationModal').open();
    }

    public onCreated(reservation: IReservation): void {
        this.reservations.push(reservation);
    }

    public onRead(): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .get(EURLS.Reservations, requestHeaders)
            .then((response: IReservation[]) => this.reservations = [...response])
            .then(() => this.isListEmpty = !this.reservations.length);
    }

    public onUpdate(reservation: IReservation): void {
        this.reservationForUpdate = reservation;
        this.ngxSmartModalService.getModal('updateReservationModal').open();
    }

    public onUpdated(reservation: IReservation): void {
        const updatedIndex = this.reservations.findIndex(oldReservation => oldReservation.id === reservation.id);
        this.reservations[updatedIndex] = reservation;
    }

    public onDelete(reservation: IReservation): void {
        this.reservationForDelete = reservation;
        this.ngxSmartModalService.getModal('deleteReservationModal').open();
    }

    public onDeleted(reservation: IReservation): void {
        const deletedIndex = this.reservations.findIndex(oldReservation => oldReservation.id === reservation.id);
        this.reservations.splice(deletedIndex, 1);
    }
}
