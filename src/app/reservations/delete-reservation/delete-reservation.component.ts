import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {EURLS, IReservation} from "../../utils";
import {AuthService} from "../../services/auth.service";
import {ApiService} from "../../services/api.service";
import {HttpHeaders} from "@angular/common/http";

@Component({
    selector: 'app-delete-reservation',
    templateUrl: './delete-reservation.component.html',
    styleUrls: ['./delete-reservation.component.scss']
})
export class DeleteReservationComponent implements OnInit {
    private reservation: IReservation;

    @Input('reservation') set _reservation(input: IReservation) {
        if (!input) {
            this.reservation = undefined;
            return;
        }
        else {
            this.reservation = input;
        }
    }

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    @Output() onDeleted = new EventEmitter<IReservation>();

    ngOnInit() {
    }

    onCancel(modal: NgxSmartModalComponent): void {
        modal.close();
    }

    onDelete(modal: NgxSmartModalComponent): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .delete(`${EURLS.Reservations}${this.reservation.id}`, requestHeaders)
            .then((response: IReservation) => {
                this.onDeleted.emit(this.reservation);
                modal.close();
            })
            .catch(error => modal.close());
    }
}
