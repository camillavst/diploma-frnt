import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {IManager} from "../../utils";
import {FormGroup} from "@angular/forms";
import {NgxSmartModalComponent} from "ngx-smart-modal";

@Component({
    selector: 'app-create-manager',
    templateUrl: './create-manager.component.html',
    styleUrls: ['./create-manager.component.scss']
})
export class CreateManagerComponent implements OnInit {
    public createManagerForm = new FormGroup({});

    @Output() onCreated = new EventEmitter<IManager>();

    constructor() {
    }

    ngOnInit() {
    }

    onSubmit(modal: NgxSmartModalComponent): void {

    }
}
