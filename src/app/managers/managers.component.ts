import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {ApiService} from "../services/api.service";
import {EURLS, IManager} from "../utils";
import {HttpHeaders} from "@angular/common/http";

@Component({
    selector: 'app-managers',
    templateUrl: './managers.component.html',
    styleUrls: ['./managers.component.scss']
})
export class ManagersComponent implements OnInit {
    public isListEmpty: boolean = true;

    public managers: IManager[] = [];

    public tableHeaders = [
        {fieldName: 'id', title: 'ID'},
        {fieldName: 'name', title: 'Name'},
        {fieldName: 'email', title: 'Email'},
        {fieldName: 'role', title: 'Role'},
    ];



    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
        this.onRead();
    }

    public onCreate(): void {

    }

    public onRead(): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .get(EURLS.Managers, requestHeaders)
            .then((response: IManager[]) => this.managers = [...response])
            .then(() => this.isListEmpty = !this.managers.length);
    }

    public onCreated(manager: IManager): void {
        this.managers.push(manager);
    }
}
