import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../services/auth.service";

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
    public isAdmin = false;

    constructor(private router: Router,
                private authService: AuthService) {
    }

    ngOnInit(): void {
        const user = this.authService.getUser();
        this.isAdmin = user && user.role === 'admin';
    }

    onLogout(): void {
        this.authService.removeAuthData();
        this.router
            .navigate(['login'])
            .then(() => true);
    }
}
