import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {ApiService} from "../services/api.service";
import {EURLS, IProduct} from "../utils";
import {HttpHeaders} from "@angular/common/http";
import {NgxSmartModalService} from "ngx-smart-modal";

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
    public isListEmpty: boolean = true;
    public productForUpdate: IProduct;
    public productForDelete: IProduct;

    public products: IProduct[] = [];

    public tableHeaders = [
        // {fieldName: 'creator', title: 'Creator'},
        {fieldName: 'name', title: 'Name'},
        {fieldName: 'category', title: 'Category'},
        {fieldName: 'supplyNumber', title: 'Supply number'},
        {fieldName: 'buyPrice', title: 'Buy price'},
        {fieldName: 'sellPrice', title: 'Sell price'},
        {fieldName: 'buyAmount', title: 'Buy amount'},
        {fieldName: 'currentAmount', title: 'Current amount'},
        {fieldName: 'validUntil', title: 'Valid until'}
    ];

    constructor(public ngxSmartModalService: NgxSmartModalService,
                private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit(): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .get(EURLS.Products, requestHeaders)
            .then((response: IProduct[]) => this.products = [...response])
            .then(() => this.isListEmpty = !this.products.length);
    }

    onCreate() {
        this.ngxSmartModalService.getModal('createProductModal').open();
    }

    onCreated(product: IProduct): void {
        this.products.push(product);
    }

    onUpdate(product: IProduct): void {
        this.productForUpdate = product;
        this.ngxSmartModalService.getModal('updateProductModal').open();
    }

    onUpdated(product: IProduct): void {
        const updatedIndex = this.products.findIndex(oldProduct => oldProduct.id === product.id);
        this.products[updatedIndex] = product;
    }

    onDelete(product: IProduct): void {
        this.productForDelete = product;
        this.ngxSmartModalService.getModal('deleteProductModal').open();
    }

    onDeleted(product: IProduct): void {
        const deletedIndex = this.products.findIndex(oldProduct => oldProduct.id === product.id);
        this.products.splice(deletedIndex, 1);
    }
}
