import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {EURLS, ICustomer, IProduct, ProductsCategories} from "../../utils";
import {HttpHeaders} from "@angular/common/http";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-create-product',
    templateUrl: './create-product.component.html',
    styleUrls: ['./create-product.component.scss']
})
export class CreateProductComponent implements OnInit {
    public customerProductForm = new FormGroup({
        name: new FormControl(),
        category: new FormControl(),
        supplyNumber: new FormControl(),
        buyPrice: new FormControl(),
        sellPrice: new FormControl(),
        buyAmount: new FormControl(),
        currentAmount: new FormControl(),
        validUntil: new FormControl()
    });

    public categories = ProductsCategories;

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    @Output() onCreated = new EventEmitter<IProduct>();

    ngOnInit() {
    }

    public onSubmit(modal: NgxSmartModalComponent): void {
        const body: ICustomer = this.customerProductForm.getRawValue();
        const headers: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .post(EURLS.Products, body, headers)
            .then((response: IProduct) => {
                this.onCreated.emit(response);
                modal.close();
            });
    }
}
