import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ApiService} from "../../services/api.service";
import {EURLS, IProduct} from "../../utils";
import {AuthService} from "../../services/auth.service";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {FormControl, FormGroup} from "@angular/forms";
import {HttpHeaders} from "@angular/common/http";

@Component({
    selector: 'app-delete-product',
    templateUrl: './delete-product.component.html',
    styleUrls: ['./delete-product.component.scss']
})
export class DeleteProductComponent implements OnInit {
    private product: IProduct;

    @Input('product') set _product(input: IProduct) {
        if (!input) {
            this.product = undefined;
            return;
        }
        else {
            this.product = input;
        }
    }

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit() {
    }

    @Output() onDeleted = new EventEmitter<IProduct>();

    onDelete(modal: NgxSmartModalComponent): void {
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .delete(`${EURLS.Products}${this.product.id}`, requestHeaders)
            .then((response: IProduct) => {
                this.onDeleted.emit(this.product);
                modal.close();
            })
            .catch(error => modal.close());
    }

    onCancel(modal: NgxSmartModalComponent): void {
        modal.close();
    }
}
