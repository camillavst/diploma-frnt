import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {EURLS, ICustomer, IProduct, ProductsCategories} from "../../utils";
import {FormControl, FormGroup} from "@angular/forms";
import {NgxSmartModalComponent} from "ngx-smart-modal";
import {HttpHeaders} from "@angular/common/http";
import {ApiService} from "../../services/api.service";
import {AuthService} from "../../services/auth.service";

@Component({
    selector: 'app-update-product',
    templateUrl: './update-product.component.html',
    styleUrls: ['./update-product.component.scss']
})
export class UpdateProductComponent implements OnInit {
    private product: IProduct;
    public categories = ProductsCategories;

    @Input('product') set _product(input: IProduct) {
        if (!input) {
            this.product = undefined;
            return;
        }
        else {
            this.product = input;
        }

        this.productUpdateForm.controls.name.setValue(input.name);
        this.productUpdateForm.controls.category.setValue(input.category);
        this.productUpdateForm.controls.supplyNumber.setValue(input.supplyNumber);
        this.productUpdateForm.controls.buyPrice.setValue(input.buyPrice);
        this.productUpdateForm.controls.sellPrice.setValue(input.sellPrice);
        this.productUpdateForm.controls.buyAmount.setValue(input.buyAmount);
        this.productUpdateForm.controls.currentAmount.setValue(input.currentAmount);
        this.productUpdateForm.controls.validUntil.setValue(input.validUntil);
    }

    public productUpdateForm = new FormGroup({
        name: new FormControl(),
        category: new FormControl(),
        supplyNumber: new FormControl(),
        buyPrice: new FormControl(),
        sellPrice: new FormControl(),
        buyAmount: new FormControl(),
        currentAmount: new FormControl(),
        validUntil: new FormControl()
    });

    constructor(private authService: AuthService,
                private apiService: ApiService) {
    }

    ngOnInit() {
    }

    @Output() onUpdated = new EventEmitter<IProduct>();

    onSubmit(modal: NgxSmartModalComponent): void {
        const body: IProduct = this.productUpdateForm.getRawValue();
        const requestHeaders: HttpHeaders = this.authService.getRequestHeaders();

        this.apiService
            .put(`${EURLS.Products}${this.product.id}`, body, requestHeaders)
            .then((response: IProduct) => {
                this.onUpdated.emit(response);
                modal.close();
            });
    }
}
